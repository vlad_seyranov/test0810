import { defineStore } from 'pinia'
import axios from "axios";

export const useAppStore = defineStore('app', {
  state: () => ({
    products: [],
  }),
  getters: {
    getProducts:(state) => state.products
  },
  actions: {
    async fetchProducts() {
      try {
        const res = await axios.get('https://fakestoreapi.com/products')
        if (res.status !== 200) {
          throw new Error(`Error status: ${res.status}`) // прокидываем ошибку
        }
      const dataFromServer = res.data;

      // Добавление поля 'list' со значением 1 к каждому элементу данных
      const dataWithListField = dataFromServer.map(item => ({
        ...item,
        list: 1
      }));

      // Обновление состояния компонента
      this.products = dataWithListField;
      console.log(this.products)
      } catch (error) {console.error('Error fetching data:', error)}
    },
    async createProduct(newProduct) {
      this.products.unshift(newProduct);
      // try {
      //   const response = await axios.post('/api/products', newProduct);
      //   this.products.unshift(response.data);
      // } catch (error) {
      //   console.error('Ошибка при создании продукта:', error);
      // }
    },
    async deleteProduct(product) {
      this.products = this.products.filter(i => i.id !== product.id)
    },
    sortProductsByRating() {
     this.products.sort((a, b) => b.rating.rate - a.rating.rate);
  }
}
})
